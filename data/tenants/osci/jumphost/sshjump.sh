#!/usr/bin/bash

if H=$(echo $SSH_ORIGINAL_COMMAND | grep -E '^[A-Za-z0-9.:@-]+$'); then
  exec ssh -A $H
else
  echo 'JumpHost: bad hostname'
  exit 1
fi

